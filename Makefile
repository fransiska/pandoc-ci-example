files = \
	index.md

output/manual.pdf: $(files)
	pandoc \
		--pdf-engine=xelatex -V CJKmainfont=IPAexGothic \
		-M date="`date "+%B %e, %Y"`" \
		-V colorlinks=true \
		--output=$@ $(files)

# Using `pandoc` with [GitLab CI/CD](https://docs.gitlab.com/ce/ci/)

[![pipeline status](https://gitlab.com/fransiska/pandoc-ci-example/badges/master/pipeline.svg)](https://gitlab.com/fransiska/pandoc-ci-example/-/commits/master)

You can use [pandoc](https://pandoc.org/), the **universal markup converter**, with GitLab CI/CD to convert documents.

GitLab CI/CD is an Infrastructure as a Service (IaaS) from GitLab, that allows you to automatically run code on GitLab's servers on every push.
For example, you can use GitLab CI/CD to convert some `file.md` to `file.pdf` (via LaTeX) and upload the results to a web host.

All examples can be found in the [.gitlab-ci.yml](.gitlab-ci.yml).

## Using `pandoc`'s Docker Images

You can directly use use docker container in GitLab CI/CD's jobs.

If you need LaTeX (because you want to convert through to PDF), you should use the `pandoc/latex` image.
Otherwise, the smaller `pandoc/core` will suffice.

It is a good idea to be explicit about the pandoc version you require, such as `pandoc/core:2.9`.
This way, any future breaking changes in pandoc will not affect your job.
You can find out whatever the latest released docker image is on [docker hub](https://hub.docker.com/r/pandoc/core/tags.)
You should avoid specifying no tag or the latest tag -- these will float to the latest image and will expose your job to potentially breaking changes.

## Simple Usage

You have to change the [entrypoint](https://docs.docker.com/engine/reference/builder/#entrypoint) of the docker image to be able to use them within the GitLab CI/CD job.

Below is a complete example to run `pandoc --help`:

```yaml
simple:
  image:
    name: pandoc
    entrypoint: ["/bin/sh", "-c"]
  script:
    pandoc --help
```

## Advanced Usage

GitLab CI/CD is more or less calling shell commands, so you can work pretty similar to regular shell scripts.
As long as it does not conflict with the `yaml` syntax.

The next Job builds multiple `.md` file to a single `.pdf` using LaTeX and uploads the resulting file to GitLab as an [Job artifact](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html), which can quickly be downloaded from GitLab:

```yaml
advanced:
  image:
    name: pandoc/latex
    entrypoint: ["/bin/sh", "-c"]
  script:
    - echo "Lorem ipsum" > lorem_1.md  # create two example files
    - echo "dolor sit amet" > lorem_2.md
    - mkdir output  # create output dir
    - pandoc --output=output/result.pdf *.md
  artifacts:
    paths:
      - output
```
